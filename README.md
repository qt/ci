A collection of CI templates used in the QT group.

## Build a docker image with kaniko

Example usage:

```yaml
include:
  - project: 'qt/ci'
    file: 'Docker.gitlab-ci.yml'

build a docker image:
    extends: .docker
    variables:
        # Adjust these as necessary
        REGISTRY: $CI_REGISTRY
        REGISTRY_USER: $CI_REGISTRY_USER
        REGISTRY_PASSWORD: $CI_REGISTRY_PASSWORD
        DOCKERFILE: Dockerfile
        DOCKER_IMAGE: $CI_REGISTRY_IMAGE
        DOCKER_TAG: latest
```
